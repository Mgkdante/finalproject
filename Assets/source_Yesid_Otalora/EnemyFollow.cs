using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyFollow : MonoBehaviour
{

    public Transform player;
    public float rotationSpd = 3.0f;
    public float moveSpd = 3.0f;

    void Start()
    {
        this.player = GameObject.FindWithTag("Player").transform;
    }

    // Update is called once per frame
    void Update()
    {
        transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(player.position - transform.position), rotationSpd * Time.deltaTime);

        transform.position += transform.forward * moveSpd * Time.deltaTime;
    }
}
