using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawn : MonoBehaviour
{
    public GameObject enemy;
    private int xPos;
    private int zPos;
    private int enemyCount;
    public int spawnCount;
        
    void Start()
    {   
        
        StartCoroutine(EnemyDrop());
    }

    IEnumerator EnemyDrop()
    {
        while(enemyCount < spawnCount)
        {
           xPos = Random.Range(1, 175);
            zPos = Random.Range(1, 350);
            Instantiate(enemy, new Vector3(xPos, 200, zPos), Quaternion.identity);
            yield return new WaitForSeconds(0.001f);
            enemyCount += 1;
        }
    }

}
