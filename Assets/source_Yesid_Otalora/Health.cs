using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Health : MonoBehaviour
{
    public float health;
    public Slider slider;
    public Text text;

    void Update()
    {
        slider.value = health;
        text.text = "Health: " + health;
        if(health <= 0)
        {
            health = 0;
            //ADD RESTART MENU
        }
    }

    void OnCollisionEnter(Collision obj)
    {
        if (obj.gameObject.tag=="Enemy")
        {
            health-=5f;
        }
    }

}