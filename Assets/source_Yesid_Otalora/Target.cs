using UnityEngine;

public class Target : MonoBehaviour
{

    public ParticleSystem prefab;

    public float health = 100f;

    public void TakeDamage(float amount)
    {
        health -= amount;
        if (health <= 0f)
        {
            Die();
            Instantiate(prefab, transform.position, transform.rotation);
        }

    }

    void Die()
    {
        Destroy(gameObject);
    }

}
